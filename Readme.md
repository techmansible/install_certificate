# install_certificate
Ansible playbooks for installing SSL Certificate.
 
# Roles
Below are the roles used in this playbook

* install_certificate -- Installs the SSL Certificate using this playbook.

# Getting Started

* Create the Directory structures required for SSL Certificate installation.
* Downloads the SSL Certificate generation binary.
* Installs SSL Certificate.